# Aplicación en Java con servidor echo para pruebas - psp-pr1-test - Realizar un programa lanzador automático de test #

*Comparto el siguiente código realizado en el Técnico Superior de Desarrollo de Aplicaciones Multiplataforma.

*Se realizó como una práctica de la asignatura de “Programación de Servicios y Procesos”, es una aplicación Java.

*Hay que tener en cuenta que sólo se pudo utilizar las tecnologías y técnicas dadas en el trimestre de la práctica.

*La práctica se realizó el 08-06-2013

===================


## Texto de la práctica ##

Durante el primer trimestre hemos visto cómo  se gestionan los procesos y cómo podemos arrancarlos y controlar su estado. 

Realizaremos un programa capaz de automatizar  la tarea de lanzar test contra un servidor, realizando las siguientes funciones: 

- Ejecutar los test contra el servidor 

- Repetir un test un cierto número de veces 

- Realizar una estadísticas del funcionamiento del test 

- Mostrar el resultado de los test realizados 

El funcionamiento del programa será el siguiente: 

1. Mostraremos un menú indicando la prueba a realizar. 

2. Introducimos el número de veces que deseamos que se haga esta prueba. 

3. Realizamos las pruebas  y mostramos el resultado final. 

4. Volvemos a mostrar el menú inicial. 

5. Este proceso se repetirá hasta que pulsemos la letra ‘S’ de salir.

## Nota ##

Incluye un servidor echo para las pruebas.

## Screenshots ##

Aplicación funcionando

[![Aplicación funcionando](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr1-test/Captura1.png)](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr1-test/Captura1.png)

Servidor echo funcionando

[![Servidor echo funcionando](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr1-test/Captura2.png)](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr1-test/Captura2.png)

## Autor ##

* Francisco José Navarro García <fran@fjnavarro.com>
* Twitter : *[@fjnavarro_](https://twitter.com/fjnavarro_)*
* Linkedin: *[https://www.linkedin.com/in/fjnavarrogarcia](https://www.linkedin.com/in/fjnavarrogarcia)*
* Blog    : *[http://www.fjnavarro.com/](http://www.fjnavarro.com/)*
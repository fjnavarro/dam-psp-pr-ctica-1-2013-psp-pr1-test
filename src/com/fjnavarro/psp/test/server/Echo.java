package com.fjnavarro.psp.test.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Echo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			// Creamos el socket Servidor
	        ServerSocket sk = new ServerSocket(8083);
	        			
			// Ponemos el proceso en un bucle infinito para aceptar peticiones
			while (true) {
				// Activamos el socket para aceptar peticiones de clientes
		        Socket cliente = sk.accept();
		        
		        // Definos los Stream para gestionar el flujo de entrada de datos
		        BufferedReader entrada = new BufferedReader(
	                 new InputStreamReader(cliente.getInputStream()));
		        
		        // Definos los Stream para gestionar el flujo de salida de datos 
		        PrintWriter salida = new PrintWriter(
		              new OutputStreamWriter(cliente.getOutputStream()),true);
		        
		        // Obtenemos el String que nos env�a el cliente
		        String datos = entrada.readLine();
		        
		        // Devolvemos el String que nos env�a el cliente - La funci�n de un servido echo. :)
		        salida.println(datos);
		        
		        // Cerramos la conexi�n con el cliente
		        cliente.close();
			  }
	   } catch (IOException e) {
	          System.out.println(e);
	   }
	}

}

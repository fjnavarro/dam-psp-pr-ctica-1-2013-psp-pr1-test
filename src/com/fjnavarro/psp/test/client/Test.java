package com.fjnavarro.psp.test.client;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String opcion;
		int repetir;
		
		// TODO Auto-generated method stub
		try{
			Menu menu = new Menu();
			
			do{
				// mostramos el men�
				menu.mostrarMenu();
				
				// obtenemos una opci�n
				opcion = menu.getOpcion();
				
				// si es una opci�n v�lida
				if(opcion.contains("1")||opcion.contains("2")||
						opcion.contains("3")||opcion.contains("4")||
						opcion.contains("5")){
					// mostramos un mensaje solicitando que eliga el n�mero de repeticiones
					menu.mostrarMRepetir();
					
					// obtenemos el n�mero de veces que queremos repetir el test
					repetir = menu.getRepetir();
					
					// Lanzamos la opcion seleccionada
					if(opcion.contains("1")){
						// Lanzamos el primer test
						Test.lanzarTest("hello, world - Brian Kernighan",repetir);
					}else if(opcion.contains("2")){
						// Lanzamos el segundo test
						Test.lanzarTest("En un lugar de la mancha... - Miguel de Cervantes",repetir);
					}else if(opcion.contains("3")){
						// Lanzamos el tercer test
						Test.lanzarTest("�Zas en toda la boca! - Peter Griffin",repetir);
					}else if(opcion.contains("4")){
						// Lanzamos el cuarto test
						Test.lanzarTest("El ignorante afirma, el sabio duda y reflexiona - Arist�teles",repetir);
					}else if(opcion.contains("5")){
						// Lanzamos el quinto test
						Test.lanzarTest("La esperanza es el sue�o del hombre despierto - Arist�teles",repetir);
					}
				}
				
				// salimos del motor de la aplicaci�n al escribir "S" o "s"
			}while(!(opcion.contains("S")||opcion.contains("s")));
			// finaliza la aplicaci�n
			System.out.println("****F�N DE LA APLICACI�N****");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/*
	 * Enviamos un texto a un servidor "echo" un n�mero de veces
	 * */
	public static void lanzarTest(String texto, int repetir){
		String strResultado;
		int contador=1;
		
		try{
			// Lanzamos el n�mero de test que se ha solicitado
			while(contador<=repetir){
				// Creamos el socket cliente
				Socket sk = new Socket("127.0.0.1", 8083);
				
				// Definos los Stream para gestionar el flujo de entrada de datos
				InputStream in = sk.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				
				// Definos los Stream para gestionar el flujo de salida de datos
				OutputStream out = sk.getOutputStream();
				PrintWriter pr = new PrintWriter(out, true);
				
				// Enviamos el texto al servidor
				pr.println(texto);
				
				// Obtenemos el resultado del servidor
				strResultado = br.readLine();
				
				// Mostramos unas estad�sticas con los resultados
				System.out.print("Ejecutado "+contador+" de "+repetir+": ");
				System.out.println(strResultado);
				
				// Cerramos la conexi�n al socket
				sk.close();
				
				// incrementamos el contador de repeticiones
				contador++;
			}
			System.out.println("****Termin� el actual lanzamiento de test****");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

package com.fjnavarro.psp.test.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Clase que gestiona el Men� principal de la aplicaci�n
 */
public class Menu {
	/**
	 * Muestra el men� principal de la aplicaci�n
	 */
	public void mostrarMenu(){
		// mostrar cabecera del men�
		System.out.println("********************************************");
		System.out.println("** Lanzador de Test seleccione una opci�n **");
		System.out.println("********************************************\n\n");
		
		// mostrar opciones de test
		System.out.println("Seleccione que test desea realizar:");
		System.out.println("***********************************");
		
		System.out.println("1 - Lanzar test echo 'hello, world - Brian Kernighan'");
		System.out.println("2 - Lanzar test echo 'En un lugar de la mancha... - Miguel de Cervantes'");
		System.out.println("3 - Lanzar test echo '�Zas en toda la boca! - Peter Griffin'");
		System.out.println("4 - Lanzar test echo 'El ignorante afirma, el sabio duda y reflexiona - Arist�teles'");
		System.out.println("5 - Lanzar test echo 'La esperanza es el sue�o del hombre despierto - Arist�teles'");
		System.out.println("****");
		System.out.println("S - Para salir de la aplicaci�n");
		
		System.out.println("***********************************");
		System.out.println("Seleccione una opci�n y pulse ENTER");		
	}
	
	/**
	 * Muestra un mensaje solicitando el insertar el n�mero de repeticiones
	 */
	public void mostrarMRepetir(){
		System.out.println("Seleccione el n�mero de veces que desea repetir el test y pulse ENTER:");
		System.out.println("**********************************************************************");		
	}
	
	/**
	 * Obtiene una cadena de texto con la opci�n elegida
	 */
	public String getOpcion(){
		String cadena = null;
		
		// objetos para obtener los caracteres introducidos desde el teclado
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader leer = new BufferedReader(isr);
		
		try{
			// leemos una cadena
			cadena = leer.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return cadena;
	}
	
	/**
	 * Obtiene el n�mero de veces que un usuario quiere repetir un test en concreto
	 */
	public int getRepetir(){
		int repetir = 0;

		try{
			// obtenemos la cadena introducida desde el teclado y la transformamos en un n�mero
			repetir = Integer.parseInt(this.getOpcion());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return repetir;
	}
}
